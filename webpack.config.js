const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const {resolve} = require('path');

module.exports = {
    entry: './src/app.js',
    output: {
        path: resolve(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },
    module: {
        rules: [
            {
                test: /.scss$/,

                use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: ["css-loader","sass-loader"],
                publicPath: "/dist"
                })
            },
            { test: /\.pug$/, use: "pug-loader" },
            // => "jade" loader is used for ".jade" files
                 ]
        },
    devServer: {
        contentBase: resolve(__dirname, "dist"),
        compress: true,
        port: 9000,
        stats:'errors-only',
        open: true
    },
    plugins: [
        new HtmlWebpackPlugin(
            {
                title: 'Custom template',
                minify:{
                  collapseWhitespace: true
                },
                hash: true,
                template: './src/index.pug'
            }
         ),
        new ExtractTextPlugin({
            filename: "app.style.css",
            disable: false,
            allChunks: true
            })
    ]

};